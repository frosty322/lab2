﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParseBDU2
{
    /// <summary>
    /// Логика взаимодействия для dialog1.xaml
    /// </summary>
    public partial class dialog1 : Window
    {
        public dialog1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            WebClient webClient = new WebClient();
            var thrlistStart = System.IO.Path.GetFullPath(@"..\..\");
            try
            {
                webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", thrlistStart + @"LocalBase\\thrlist.xlsx");

            }
            catch (WebException a)
            {
                Erorr erorr = new Erorr();
                erorr.Show();
                this.Close();
                return;
            }
            MainPart mainPart = new MainPart();
            mainPart.Show();
            this.Close();


        }
    }
}
