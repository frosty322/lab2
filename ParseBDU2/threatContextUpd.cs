﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace ParseBDU2
{
    class threatContextUpd
    {
        public List<threat> threats = new List<threat>();
        Excel.Application ex = new Excel.Application();

        public void AddingThreatUpd(string a, List<threat> OldThreats)
        {
            var thrlistStart = System.IO.Path.GetFullPath(@"..\..\");
            var thrlist = thrlistStart + a;
            Excel.Workbook workBook = ex.Workbooks.Open(thrlist,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing);
            Excel.Worksheet worksheet = (Excel.Worksheet)workBook.Sheets[1];
            for (int i = 3; i < worksheet.UsedRange.Rows.Count + 1; i++)
            {
                if (worksheet.Cells[i, 10].Value.Equals(OldThreats[i - 3].Date))
                {
                    threats.Add(new threat
                    {
                        Id = (int)worksheet.Cells[i, 1].Value,
                        Name = worksheet.Cells[i, 2].Value,
                        Disc = worksheet.Cells[i, 3].Value,
                        Sours = worksheet.Cells[i, 4].Value,
                        Obj = worksheet.Cells[i, 5].Value,
                        Conf = (int)worksheet.Cells[i, 6].Value,
                        integ = (int)worksheet.Cells[i, 7].Value,
                        Avail = (int)worksheet.Cells[i, 8].Value,
                        Date = worksheet.Cells[i, 10].Value
                    });
                }
                else
                {
                    threats.Add(new threat()
                    {
                        Id = (int)worksheet.Cells[i, 1].Value,
                        Name = worksheet.Cells[i, 2].Value,
                        Disc = worksheet.Cells[i, 3].Value,
                        Sours = worksheet.Cells[i, 4].Value,
                        Obj = worksheet.Cells[i, 5].Value,
                        Conf = (int)worksheet.Cells[i, 6].Value,
                        integ = (int)worksheet.Cells[i, 7].Value,
                        Avail = (int)worksheet.Cells[i, 8].Value,
                        Date = worksheet.Cells[i, 10].Value
                    });
                    if (OldThreats[i - 3].Name != worksheet.Cells[i, 2].Value)
                    {

                        threats[i - 3].Name = $"UPD! Было: {OldThreats[i - 3].Name} Стало: {(string)worksheet.Cells[i, 2].Value2}";


                    }
                    if (OldThreats[i - 3].Disc != worksheet.Cells[i, 3].Value)
                    {
                        threats[i - 3].Disc = "Было: " + OldThreats[i - 3].Disc + " Стало: " + (string)worksheet.Cells[i, 3].Value;


                    }
                    if (OldThreats[i - 3].Sours != worksheet.Cells[i, 4].Value)
                    {
                        threats[i - 3].Sours = "Было: " + OldThreats[i - 3].Sours + " Стало: " + (string)worksheet.Cells[i, 4].Value;


                    }
                    if (OldThreats[i - 3].Obj != worksheet.Cells[i, 5].Value)
                    {
                        threats[i - 3].Obj = "Было: " + OldThreats[i - 3].Obj + " Стало: " + (string)worksheet.Cells[i, 5].Value;


                    }
                    if (OldThreats[i - 3].Conf != worksheet.Cells[i, 6].Value)
                    {
                        threats[i - 3].Conf = worksheet.Cells[i, 6].Value;


                    }
                    if (OldThreats[i - 3].integ != worksheet.Cells[i, 7].Value)
                    {
                        threats[i - 3].integ = worksheet.Cells[i, 7].Value;


                    }
                    if (OldThreats[i - 3].Avail != worksheet.Cells[i, 8].Value)
                    {
                        threats[i - 3].Avail = worksheet.Cells[i, 3].Value;
                    }

                }
                if ((int)worksheet.Cells[i, 6].Value == 1)
                {
                    threats[i - 3].Confstr = "Да";
                }
                else
                {
                    threats[i - 3].Confstr = "Нет";
                }
                if ((int)worksheet.Cells[i, 7].Value == 1)
                {
                    threats[i - 3].integstr = "Да";
                }
                else
                {
                    threats[i - 3].integstr = "Нет";
                }
                if ((int)worksheet.Cells[i, 8].Value == 1)
                {
                    threats[i - 3].Availstr = "Да";
                }
                else
                {
                    threats[i - 3].Availstr = "Нет";
                }
            }
            workBook.Close();
            ex.Quit();
            var process = Process.GetProcessesByName("EXCEL");
            process[0].Kill();


        }
    }
}
