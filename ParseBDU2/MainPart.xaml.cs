﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParseBDU2
{
    /// <summary>
    /// Логика взаимодействия для MainPart.xaml
    /// </summary>
    public partial class MainPart : Window
    {
        static threatContext threatsList = new threatContext();
        static threatContextUpd threatsListUpd = new threatContextUpd();
        static int start = 0;
        static int end = 20;
        static int max; 
        
        //ListOfThreats.ItemsSource = threatsList.threats;

        public MainPart()
        {
            
            WaitWindow _wait = new WaitWindow();
            _wait.Show();
            InitializeComponent();
            threatsList.AddingThreat(@"LocalBase\\thrlist.xlsx");
            _wait.Close();
            max = threatsList.threats.Count;
            
            
            ThreatPageList(threatsList.threats, start, end);
        }

        private void ThreatPageList(List<threat> _threats, int start, int end)
        {
            if (max - end < 0)
            {
                while(max-end <= 0)
                {
                    end--;
                }
            }
            threatContext threatContext20 = new threatContext();
            for (int i = start; i < end; i++)
            {
                threatContext20.threats.Add(_threats[i]);
            }
            ListOfThreats.ItemsSource = threatContext20.threats;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WebClient webClient = new WebClient();
            var thrlistStart = System.IO.Path.GetFullPath(@"..\..\");
            try
            {
                webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", thrlistStart + @"LocalBase\\thrlist.xlsx");

            }
            catch (WebException a)
            {
                Erorr erorr = new Erorr();
                erorr.Show();
                this.Close();
                return;
            }
            WaitWindow _wait = new WaitWindow();
            _wait.Show();
            threatsListUpd.AddingThreatUpd(@"LocalBase\\thrlist.xlsx", threatsList.threats);
            for (int i = 0; i < threatsListUpd.threats.Count; i++)
                threatsList.threats[i] = threatsListUpd.threats[i];
            _wait.Close();
            start = 0;
            end = 20;
            ThreatPageList(threatsList.threats, start, end);
        }

        private void ListOfThreats_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (start >= 20)
            {
                start -= 20;
                end -= 20;
            }
            ThreatPageList(threatsList.threats, start, end);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (max > end)
            {
                start += 20;
                end += 20;
            }
            ThreatPageList(threatsList.threats, start, end);
        }
    }
}
