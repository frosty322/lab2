﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseBDU2
{
    class threat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Disc { get; set; }
        public string Sours { get; set; }
        public string Obj { get; set; }
        public int Conf { get; set; }
        public int integ { get; set; }
        public int Avail { get; set; }
        public string Confstr { get; set; }
        public string integstr { get; set; }
        public string Availstr { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return "УБИ" + Id.ToString() + " " + Name;
        }
    }

    
}
